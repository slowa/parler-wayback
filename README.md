# parler wayback coverage

Here's the Tweet mentioning the Parler post URLs:

    https://twitter.com/donk_enby/status/1348281459031814146

Fetch them with:

    wget --mirror --convert-links --adjust-extension --page-requisites --no-parent https://donk.sh/06d639b2-0252-4b1e-883b-f275eff7e792/

Combine all the URLs into one:

    cat donk.sh/06d639b2-0252-4b1e-883b-f275eff7e792/*txt* | grep 'https://parler.com/post' |  sort | uniq > parler-urls.txt 

Get the list of posts in the IA Wayback Machine:

    python3 ./parler-wayback.py | sort | uniq > parler-wayback-urls.txt

