import re
import csv
import gzip
import wayback

wb = wayback.WaybackClient()

fieldnames = ['key', 'url', 'timestamp', 'status_code', 'mime_type', 'raw_url', 'view_url', 'digest', 'length']

out = csv.DictWriter(gzip.open('parler-wayback.csv.gz', 'wt'), fieldnames=fieldnames)
out.writeheader()

# adjusting the limit here causes more/less results to come back sometimes
# before throwing network errors
# more context at: https://github.com/edgi-govdata-archiving/wayback/issues/65

for result in wb.search('parler.com/post/', filter="mimetype:.*html", matchType='prefix', limit=200_000):
    m = re.match(r'^https?://parler.com/post/[0-9a-f]+$', result.url)
    if m and result.status_code == 200:
        r = result._asdict()
        out.writerow(result._asdict())
